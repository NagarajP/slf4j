// console: if we define both 'slf4j-jdk14' and 'logback classic' dependencies in pom.xml
/*
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/C:/Users/nagaraj.pattar/.m2/repository/org/slf4j/slf4j-jdk14/1.7.25/slf4j-jdk14-1.7.25.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/C:/Users/nagaraj.pattar/.m2/repository/ch/qos/logback/logback-classic/1.2.3/logback-classic-1.2.3.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.slf4j.impl.JDK14LoggerFactory]
Jan 07, 2020 12:17:39 PM slf4j.Slf4jLogger main
INFO: logger info main
print main
*/

package slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4jLogger {

//	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
	private static final Logger logger = LoggerFactory.getLogger(Slf4jLogger.class);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		logger.debug("debug main");
//		logger.trace("trace main");
		logger.info("logger info main");
		System.out.println("print main");
	}

}
